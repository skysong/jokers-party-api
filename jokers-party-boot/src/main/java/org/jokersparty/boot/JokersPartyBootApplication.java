package org.jokersparty.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JokersPartyBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(JokersPartyBootApplication.class, args);
    }

}
