package org.jokersparty.boot.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class SelectTweetPageListDTO implements Serializable {

    /**
     * The ID of the User to lookup.
     */
    private String userId;

    /**
     * This parameter is used to get the next page of results.
     */
    private String paginationToken;

    /**
     * The maximum number of results.
     */
    @NotNull
    private Integer maxResults = 10;

}
