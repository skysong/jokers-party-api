package org.jokersparty.boot.biz;

import com.google.common.collect.Sets;
import com.twitter.clientlib.ApiException;
import com.twitter.clientlib.api.TwitterApi;
import com.twitter.clientlib.model.Get2UsersIdTweetsResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jokersparty.boot.model.dto.SelectTweetPageListDTO;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class TwitterBiz {

    private TwitterApi twitterApi;

    public Get2UsersIdTweetsResponse selectTweetPageList(SelectTweetPageListDTO req) throws ApiException {
        Get2UsersIdTweetsResponse get2UsersIdTweetsResponse = twitterApi.tweets()
                .usersIdTweets(req.getUserId())
                .exclude(Sets.newHashSet("replies"))
                .paginationToken(req.getPaginationToken())
                .maxResults(req.getMaxResults()).execute();
        log.info("Query tweet, response data: {}", get2UsersIdTweetsResponse.toJson());
        return get2UsersIdTweetsResponse;
    }

}
