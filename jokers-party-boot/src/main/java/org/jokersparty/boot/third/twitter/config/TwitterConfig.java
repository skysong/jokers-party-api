package org.jokersparty.boot.third.twitter.config;

import com.twitter.clientlib.TwitterCredentialsBearer;
import com.twitter.clientlib.api.TweetsApi;
import com.twitter.clientlib.api.TwitterApi;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class TwitterConfig {

    @Value("${twitter.bearerToken}")
    private String bearerToken;

    @Bean
    TwitterApi twitterApi() {
        TwitterCredentialsBearer twitterCredentialsBearer = new TwitterCredentialsBearer(bearerToken);
        TwitterApi twitterApi = new TwitterApi(twitterCredentialsBearer);
        return twitterApi;
    }

    @Bean
    TweetsApi tweetsApi() {
        return twitterApi().tweets();
    }

}
