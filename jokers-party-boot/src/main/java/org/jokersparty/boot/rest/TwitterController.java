package org.jokersparty.boot.rest;

import com.twitter.clientlib.ApiException;
import com.twitter.clientlib.model.Get2UsersIdTweetsResponse;
import lombok.AllArgsConstructor;
import org.jokersparty.boot.biz.TwitterBiz;
import org.jokersparty.boot.model.dto.SelectTweetPageListDTO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class TwitterController {

    private TwitterBiz twitterBiz;

    @GetMapping("/api/twitter/selectTweetPageList")
    public Get2UsersIdTweetsResponse selectTweetPageList(@Validated SelectTweetPageListDTO req) throws ApiException {
        return twitterBiz.selectTweetPageList(req);
    }

}
