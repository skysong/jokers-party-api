package org.jokersparty.boot.config;

import lombok.extern.slf4j.Slf4j;
import org.jokersparty.boot.interceptor.CorsInterceptor;
import org.jokersparty.boot.interceptor.OptionsInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Slf4j
@Configuration
public class CustomMvcConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new CorsInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(new OptionsInterceptor()).addPathPatterns("/**");
    }

}
