package org.jokersparty.boot.biz;

import com.twitter.clientlib.ApiException;
import com.twitter.clientlib.model.Get2UsersIdTweetsResponse;
import org.apache.commons.lang3.StringUtils;
import org.jokersparty.boot.model.dto.SelectTweetPageListDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TwitterBizTest {

    @Autowired
    private TwitterBiz twitterBiz;

    @Test
    public void selectTweetPageListTest() throws ApiException {
        String userId = "2244994945";
        Integer maxResults = 100;

        boolean hasNextPage = true;
        String nextToken = null;

        while (hasNextPage) {
            SelectTweetPageListDTO selectTweetPageListDTO = new SelectTweetPageListDTO();
            selectTweetPageListDTO.setUserId(userId);
            selectTweetPageListDTO.setPaginationToken(nextToken);
            selectTweetPageListDTO.setMaxResults(maxResults);
            Get2UsersIdTweetsResponse get2UsersIdTweetsResponse = twitterBiz.selectTweetPageList(selectTweetPageListDTO);
            if (StringUtils.isNoneBlank(get2UsersIdTweetsResponse.getMeta().getNextToken())) {
                nextToken = get2UsersIdTweetsResponse.getMeta().getNextToken();
            } else {
                hasNextPage = false;
            }
        }

    }

}
