<div align="center">
<h1>Jokers Party Api</h1>
</div>

<p align="center">
    <img src="https://img.shields.io/badge/Spring%20Boot-2.7.4-blue">
    <img src="https://img.shields.io/badge/JDK-1.8+-green.svg">
    <img src="https://img.shields.io/badge/license-Apache%202-blue.svg">
</p>

### 简介

Jokers Party Api是Jokers Party Website服务端Api

```

一、打包命令：指定prod环境，进行打包
mvn clean package -DskipTests -Pprod

二、启动
    2、Linux环境，运行./start.sh

```